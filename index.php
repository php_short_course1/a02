<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>A02</title>
</head>
<body>



	<h2>Numbers that are divisible by 5</h2>
	
	<p><?php divisibleByFive() ?></p>




	<h2>Array Manipulation</h2>

	<p><?php array_push($students, 'John Smith'); ?></p>
	<p>array(1) <?php print_r($students); ?></p>

	<p><?php echo count($students); ?></p>

	<p><?php array_push($students, 'Jane Smith'); ?></p>
	<p>array(2) <?php print_r($students); ?></p>

	<p><?php echo count($students); ?></p>

	<p><?php array_shift($students); ?></p>
	<p>array(1) <?php print_r($students); ?></p>

	<p><?php echo count($students); ?></p>


</body>
</html>
